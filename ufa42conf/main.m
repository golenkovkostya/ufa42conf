//
//  main.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UCAppDelegate class]));
    }
}
