//
//  UCSharedClient.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCSharedClient : NSObject

+ (instancetype) sharedClient;

- (void)sendRequestToUrl:(NSString *)url isPost:(BOOL)isPost data:(NSDictionary *)dictionary withCopletionBlock:(void(^)(NSMutableDictionary* data, NSError* error))block;

@end
