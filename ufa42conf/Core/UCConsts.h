//
//  UCConsts.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef ufa42conf_UCConsts_h
#define ufa42conf_UCConsts_h

#pragma mark request URLS
static NSString * const kUCBaseURL = @"http://ufa42conf.org/api/";
static NSString * const kUCEventURL = @"events";
static NSString * const kUCAnswersURL = @"poll";

#pragma mark Social profile URLS
static NSString* const kUCTwitterProfileURL = @"https://mobile.twitter.com/ufa42conf";

#endif
