//
//  UCSharedClient.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCSharedClient.h"

static NSTimeInterval const kTimeout = 30;
static NSInteger const kMaxConcurrentOperationCount = 1;

@interface UCSharedClient() {
    NSOperationQueue* operationQueue;
}
@end

@implementation UCSharedClient

+ (instancetype)sharedClient {
    static UCSharedClient *sharedClient;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedClient = [[UCSharedClient alloc] init];
    });
    
    return sharedClient;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue setMaxConcurrentOperationCount: kMaxConcurrentOperationCount];
    }
    return self;
}

- (void)sendRequestToUrl:(NSString *)url isPost:(BOOL)isPost data:(NSDictionary *)dictionary withCopletionBlock:(void (^)(NSMutableDictionary *, NSError *))block {
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:kTimeout];
    if (isPost) {
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        NSData* body = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
        [request setHTTPBody:body];
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    });
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:operationQueue
                           completionHandler:^(NSURLResponse* _response, NSData* data, NSError* connectionError) {
                               NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               NSHTTPURLResponse* response = (NSHTTPURLResponse *) _response;
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                               });
                               //NSURLErrorDomain
                               if (connectionError) {
                                   if (response.statusCode ) {
                                       connectionError = [NSError errorWithDomain:@"ufa42conf.org" code:0
                                                                         userInfo:@{@"errorDesc": [NSString stringWithFormat:@"%@", body]}];
                                   } else {
                                       connectionError = [NSError errorWithDomain:@"ufa42conf.org" code:response.statusCode
                                                                         userInfo:@{@"errorDesc": [NSString stringWithFormat:@"%@", body]}];
                                   }
                                   block(nil, connectionError);
                               } else {
                                   
                                   id jsonData = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:NSJSONReadingMutableContainers
                                                                                   error:nil];
//                                   id result = [jsonData valueForKey:@"response"];
                                   
                                   block(jsonData, nil);
                               }
                               
                           }];
}

@end
