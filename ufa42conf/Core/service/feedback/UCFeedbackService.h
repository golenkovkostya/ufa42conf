//
//  UCFeedbackService.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 19/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCService.h"
#import "UCServiceProtocol.h"

@interface UCFeedbackService : UCService<UCServiceProtocol>

- (void)sendAnswers:(NSDictionary *)answers;

@end
