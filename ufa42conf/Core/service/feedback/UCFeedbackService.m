//
//  UCFeedbackService.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 19/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCFeedbackService.h"

@implementation UCFeedbackService

+ (instancetype)sharedService {
    static UCFeedbackService* service;
    static dispatch_once_t oncePredicat;
    dispatch_once(&oncePredicat, ^{
        service = [[UCFeedbackService alloc] init];
    });
    return  service;
}

- (void)sendAnswers:(NSDictionary *)answers {
    [self clientSendPostRequestToURL:kUCAnswersURL data:@{@"answers":answers} withCompletionBlock:^BOOL(NSMutableDictionary *data, NSError * error) {
        return NO;
    }];
}

@end
