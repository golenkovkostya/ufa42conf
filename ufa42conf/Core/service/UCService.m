//
//  UCService.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCService.h"
#import "UCSharedClient.h"

@implementation UCService

- (void)clientSendRequestToURL:(NSString *)url withCompletionBlock:(BOOL (^)(NSMutableDictionary *, NSError *))block {
    
    void (^clientHandler)(NSMutableDictionary *, NSError *) = ^(NSMutableDictionary * data, NSError * error) {
        if (error) {
            if (!block(nil, error)) {
                [self showErrorMessageWithError:error];
            }
        } else {
            block(data, nil);
        }
    };
    
    [[UCSharedClient sharedClient] sendRequestToUrl:[NSString stringWithFormat:@"%@%@",kUCBaseURL,url] isPost:NO data:nil withCopletionBlock:clientHandler];
}

- (void)clientSendPostRequestToURL:(NSString *)url data:(NSDictionary *)dictionary withCompletionBlock:(BOOL (^)(NSMutableDictionary *, NSError *))block {
    void (^clientHandler)(NSMutableDictionary *, NSError *) = ^(NSMutableDictionary * data, NSError * error) {
        if (error) {
            if (!block(nil, error)) {
                [self showErrorMessageWithError:error];
            }
        } else {
            block(data, nil);
        }
    };
    [[UCSharedClient sharedClient] sendRequestToUrl:[NSString stringWithFormat:@"%@%@",kUCBaseURL,url] isPost:YES data:dictionary withCopletionBlock:clientHandler];
}

- (void) showErrorMessageWithError:(NSError *)error {
    NSString *message = @"Проблемы с сервером.";
    switch ((int)error.code) {
        case 0:
            message = @"Проблемы с интернетом.";
            break;
        default:
            break;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Ошибка"
                              message: message
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    });
}

@end
