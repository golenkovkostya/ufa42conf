//
//  UCServiceProtocol.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 18/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UCServiceProtocol <NSObject>

+ (instancetype)sharedService;

@end
