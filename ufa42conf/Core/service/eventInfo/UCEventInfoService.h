//
//  UCEventInfoService.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 18/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCService.h"
#import "UCServiceProtocol.h"
#import "UCEventInfo.h"

@interface UCEventInfoService : UCService <UCServiceProtocol>

- (void) eventInfoWithCopletionBlock:(BOOL(^)(UCEventInfo *eventInfo, NSError *error))block;

@end
