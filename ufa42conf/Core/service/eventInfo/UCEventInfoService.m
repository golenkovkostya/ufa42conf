//
//  UCEventInfoService.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 18/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCEventInfoService.h"

@implementation UCEventInfoService

+ (instancetype)sharedService {
    static UCEventInfoService* service;
    static dispatch_once_t oncePredicat;
    dispatch_once(&oncePredicat, ^{
        service = [[UCEventInfoService alloc] init];
    });
    return  service;
}

- (void)eventInfoWithCopletionBlock:(BOOL (^)(UCEventInfo *, NSError *))block {
    [self clientSendRequestToURL:kUCEventURL withCompletionBlock:^BOOL(NSMutableDictionary *data, NSError *error) {
        if (error) {
            return block(nil, error);
        } else {
            NSArray *events = [data valueForKey:@"events"];
            NSDictionary *jsonEvent = events[0];
            UCEventInfo *eventInfo = [[UCEventInfo alloc] init];
            
            [eventInfo setValuesForKeysWithDictionary:jsonEvent];
            
            return block(eventInfo,nil);
        }
    }];
}

@end
