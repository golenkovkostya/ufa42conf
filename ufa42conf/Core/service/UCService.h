//
//  UCService.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCService : NSObject

- (void) clientSendRequestToURL:(NSString *) url  withCompletionBlock:(BOOL(^)(NSMutableDictionary* data, NSError *error))block;

- (void) clientSendPostRequestToURL:(NSString *) url data:(NSDictionary *)dictionary withCompletionBlock:(BOOL(^)(NSMutableDictionary* data, NSError *error))block;

@end
