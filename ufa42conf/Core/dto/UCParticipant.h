//
//  UCParticipant.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCParticipant : NSObject

/*
 
 id : 16135676
 name : "@abdullin"
 fullname : "Rinat Abdullin"
 avatarUrl : "http://pbs.twimg.com/profile_images/3479036762/40c99d96aa9a4e57cfa7d54d1fb7d5b2.jpeg"
 
 */

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *avatarUrl;

@end
