//
//  UCQuestion.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCSimpleType.h"

@interface UCQuestion : UCSimpleType

//@property (nonatomic, strong) NSNumber *id;
//@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSMutableArray *answers;
@property (nonatomic, strong) NSNumber *answerLimit;

@end
