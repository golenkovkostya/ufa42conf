//
//  UCTalk.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCParticipant.h"

@interface UCTalk : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) UCParticipant *speaker;
@property (nonatomic, strong) NSString *slidesUrl;

@end
