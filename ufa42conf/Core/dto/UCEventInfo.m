//
//  UCEventInfo.m
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCEventInfo.h"

@implementation UCEventInfo

- (void)setTalksPlace:(NSDictionary *)jsonTalksPlace {
    _talksPlace = [[UCPlace alloc] init];
    [_talksPlace setValuesForKeysWithDictionary:jsonTalksPlace];
}

- (void)setBeersPlace:(NSDictionary *)jsonBeersPlace {
    _beersPlace = [[UCPlace alloc] init];
    [_beersPlace setValuesForKeysWithDictionary:jsonBeersPlace];
}

- (void)setTalks:(NSMutableArray *)jsonTalks {
    _talks = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonTalk in jsonTalks) {
        UCTalk *talk = [[UCTalk alloc]init];
        [talk setValuesForKeysWithDictionary:jsonTalk];
        [_talks addObject:talk];
    }
}

- (void)setLightningTalks:(NSMutableArray *)jsonLightningTalks {
    _lightningTalks = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonTalk in jsonLightningTalks) {
        UCTalk *talk = [[UCTalk alloc]init];
        [talk setValuesForKeysWithDictionary:jsonTalk];
        [_lightningTalks addObject:talk];
    }
}

- (void)setAttendees:(NSMutableArray *)jsonAttendees {
    _attendees = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonParticipant in jsonAttendees) {
        UCParticipant *participant = [[UCParticipant alloc] init];
        [participant setValuesForKeysWithDictionary:jsonParticipant];
        [_attendees addObject:participant];
    }
}

- (void)setQuestions:(NSMutableArray *)jsonQuestions {
    _questions = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonQuestion in jsonQuestions) {
        UCQuestion *question = [[UCQuestion alloc] init];
        [question setValuesForKeysWithDictionary:jsonQuestion];
        [_questions addObject:question];
    }
}

- (NSUInteger)getParticipantsCount {
    return self.attendees.count;
}

@end
