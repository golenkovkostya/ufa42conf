//
//  UCPlace.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCPlace : NSObject
/*
 
 title : "ШБ Синергия"
 lat : 54.7252452
 lon : 55.949416
 address : "Уфа, ул. Коммунистическая, 54"
 description : ""
 
 
 */

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lon;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *description;

@end
