//
//  UCQuestion.m
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCQuestion.h"

@implementation UCQuestion

- (void)setAnswers:(NSMutableArray *)jsonAnswers {
    _answers = [[NSMutableArray alloc] init];
    for (NSDictionary *jsonAnswer in jsonAnswers) {
        UCSimpleType *answer = [[UCSimpleType alloc] init];
        [answer setValuesForKeysWithDictionary:jsonAnswer];
        [_answers addObject:answer];
    }
}

@end
