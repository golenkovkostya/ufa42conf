//
//  UCEventInfo.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCPlace.h"
#import "UCTalk.h"
#import "UCParticipant.h"
#import "UCQuestion.h"

@interface UCEventInfo : NSObject

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSNumber *time;
@property (nonatomic, strong) UCPlace *talksPlace;
@property (nonatomic, strong) UCPlace *beersPlace;
@property (nonatomic, strong) NSMutableArray *talks;
@property (nonatomic, strong) NSMutableArray *lightningTalks;
@property (nonatomic, strong) NSMutableArray *attendees;
@property (nonatomic, strong) NSMutableArray *questions;

- (NSUInteger)getParticipantsCount;

@end
