//
//  UCSimpleType.h
//  ufa42conf
//
//  Created by Константин Голенков on 17.06.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCSimpleType : NSObject

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSString *text;

@end
