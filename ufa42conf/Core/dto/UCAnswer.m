//
//  UCAnswer.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 19/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCAnswer.h"

@implementation UCAnswer

- (instancetype)initWithQuestionId:(NSNumber *)questionId answerId:(NSNumber *)answerId {
    self = [super init];
    if(self) {
        self.questionId = questionId;
        self.responseId = answerId;
    }
    return self;
}

- (NSDictionary *)toDictionary {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:self.questionId forKeyPath:@"questionId"];

    [dict setValue: self.responseId forKeyPath:@"responseId"];
    return dict;
}

@end
