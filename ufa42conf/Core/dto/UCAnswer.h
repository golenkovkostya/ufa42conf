//
//  UCAnswer.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 19/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UCAnswer : NSObject

- (instancetype)initWithQuestionId:(NSNumber *)questionId answerId:(NSNumber *)answerId;

- (NSDictionary *)toDictionary;

@property (nonatomic, strong) NSNumber *questionId;
@property (nonatomic, strong) NSNumber *responseId;


@end
