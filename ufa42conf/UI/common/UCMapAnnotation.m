//
//  UCMKAnnotationView.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCMapAnnotation.h"

@implementation UCMapAnnotation

- (id)initWithTitle:(NSString *)title subTitle:(NSString *)subTitle longitudeValue:(NSNumber *)longitude latitudeValue:(NSNumber *)latitude imageName:(NSString *)imageName {
    self = [super init];
    if (self) {
        [self setCoordinate:CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue])];
        [self setTitle:title];
        [self setSubtitle:subTitle];
        self.imageName = imageName;
    }
    return self;
}

@end
