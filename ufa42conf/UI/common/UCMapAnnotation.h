//
//  UCMKAnnotationView.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface UCMapAnnotation : MKPointAnnotation

@property (nonatomic, strong) NSString *imageName;

- (id) initWithTitle:(NSString *) title subTitle:(NSString *)subTitle longitudeValue:(NSNumber *)longitude latitudeValue:(NSNumber *)latitude imageName:(NSString *)imageName;

@end
