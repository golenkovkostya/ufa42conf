//
//  UCFeedbackViewController.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UCQuestion.h"

@interface UCFeedbackViewController : UIViewController

@property (nonatomic, strong) NSArray *questions;

@end
