//
//  UCFeedbackViewController.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCFeedbackViewController.h"
//#import "UCMainViewController.h"
#import "UCAnswer.h"
#import "UCFeedbackService.h"

@interface UCFeedbackViewController (){
    NSInteger currentQuestionIndex;
}

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (nonatomic, strong) NSMutableDictionary *answers;
@property (nonatomic, strong) UCQuestion *curentQuestion;
@end

@implementation UCFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.answers = [[NSMutableDictionary alloc] init];
//    UCMainViewController *mainController = (UCMainViewController *) self.tabBarController;
//    self.questions = mainController.eventInfo.questions;
    currentQuestionIndex = 0;
    [self loadQuestion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadQuestion {
    if (self.questions.count > 0 ) {
        self.curentQuestion = self.questions[currentQuestionIndex];
        self.questionLabel.text =  self.curentQuestion.text;
    } else {
        self.questionLabel.text = @"На данный момент вопросы еще не составлены.";
        self.noBtn.hidden = YES;
        self.yesBtn.hidden = YES;
    }
}

- (void) answer:(NSNumber *) answer {
    [self.answers setObject:[answer stringValue] forKey:self.curentQuestion.id];
    currentQuestionIndex++;
    if (currentQuestionIndex < self.questions.count) {
        [self loadQuestion];
    } else {
//        self.questionLabel.text = @"Thank you very much for your answers! \n\n They will help us be better.";
        self.questionLabel.text = @"Спасибо большое за ваши ответы! \n\n Они помогут нам стать лучше.";
        self.noBtn.hidden = YES;
        self.yesBtn.hidden = YES;

        __weak typeof(self) weakSelf = self;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[UCFeedbackService sharedService] sendAnswers:weakSelf.answers];
        });
        
    }
}

- (IBAction)noAnswerAction:(id)sender {
    [self answer:[NSNumber numberWithInt:0]];
}

- (IBAction)yesAnswerAction:(id)sender {
    [self answer:[NSNumber numberWithInt:1]];
}

@end
