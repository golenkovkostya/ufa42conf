//
//  UCAboutUsViewController.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCAboutUsViewController.h"

@interface UCAboutUsViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation UCAboutUsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.webView loadHTMLString:[[NSBundle mainBundle] localizedStringForKey:@"aboutUs" value:nil
                                                                        table:@"AboutUs"] baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


@end
