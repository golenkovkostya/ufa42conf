//
//  UCNewsViewController.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "UCNewsViewController.h"
#import "UCMapAnnotation.h"
#import "UCEventInfoService.h"
#import "UCPlace.h"
//#import "UCMainViewController.h"

@interface UCNewsViewController ()

@end

@implementation UCNewsViewController

+ (instancetype)newsViewController {
    UCNewsViewController *newsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]
                                    instantiateViewControllerWithIdentifier:@"NewsVC"];
    
    return newsVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];

//    __weak typeof(self) weakSelf = self;
//
//    [[UCEventInfoService sharedService] eventInfoWithCopletionBlock:^BOOL(UCEventInfo *eventInfo, NSError *error) {
////        weakSelf.eventInfo = eventInfo;
////        UCMainViewController *mainController = (UCMainViewController *) weakSelf.tabBarController;
////        mainController.eventInfo = eventInfo;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf configureNewsView];
//        });
//        return YES;
//    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) configureNewsView {
//    self.partisipantCountLabel.text = [NSString stringWithFormat:@"Already involved %d partisipants", [self.eventInfo getParticipantsCount]];
//    [self addPlaceMark:self.eventInfo.talksPlace imageName:@"man"];
//    [self addPlaceMark:self.eventInfo.beersPlace imageName:@"beer"];
//    [self gotoLocationWithLongitude:self.eventInfo.talksPlace];
}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//
//    if ([annotation isKindOfClass:[MKUserLocation class]]) {
//        return nil;
//    }
//    
//    if ([annotation isKindOfClass:[UCMapAnnotation class]]) {
//        
//        UCMapAnnotation *myannotation = (UCMapAnnotation *)annotation;
//        
//        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
//        if (!pinView) {
//            pinView = [[MKAnnotationView alloc] initWithAnnotation:myannotation reuseIdentifier:@"CustomPinAnnotationView"];
//
//            pinView.canShowCallout = YES;
//            pinView.image = [UIImage imageNamed:myannotation.imageName];
//            //pinView.calloutOffset = CGPointMake(0, 32);
//        } else {
//            pinView.annotation = annotation;
//        }
//        return pinView;
//    }
//    return nil;
//}
//
//- (void) addPlaceMark:(UCPlace *)place imageName:(NSString *)imageName {
//    
//    UCMapAnnotation *annotation = [[UCMapAnnotation alloc] initWithTitle:place.title
//                                                                subTitle:place.address
//                                                          longitudeValue:place.lon
//                                                           latitudeValue:place.lat
//                                                               imageName:imageName];
//    
//    [self.mapView addAnnotation:annotation];
//}
//
//- (void)gotoLocationWithLongitude:(UCPlace *)place {
//    MKCoordinateRegion newRegion;
//    newRegion.center.latitude = [place.lat doubleValue];
//    newRegion.center.longitude = [place.lon doubleValue];
//    newRegion.span.latitudeDelta = 0.008;
//    newRegion.span.longitudeDelta = 0.008;
//    
//    [self.mapView setRegion:newRegion animated:YES];
//
//}


@end
