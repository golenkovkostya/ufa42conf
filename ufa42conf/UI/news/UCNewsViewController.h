//
//  UCNewsViewController.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 17/06/14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UCNewsViewController : UIViewController

+ (instancetype)newsViewController;

@end
