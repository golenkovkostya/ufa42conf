//
//  UCTabView.h
//  ufa42conf
//
//  Created by Konstantin Golenkov on 28.10.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UCTabViewProtocol;

@interface UCTabView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (nonatomic, readonly) NSUInteger tabIndex;
@property (nonatomic, getter = iSActiveTab) BOOL activeTab;
@property (weak, nonatomic) id<UCTabViewProtocol> delegate;

+ (instancetype)tabViewWithIconName:(NSString *)iconName title:(NSString *)title tabIndex:(NSUInteger)index;

@end

@protocol UCTabViewProtocol <NSObject>

- (void)tabSelected:(UCTabView *)tab;

@end