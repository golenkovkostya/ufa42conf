//
//  UCTabView.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 28.10.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCTabView.h"

static CGFloat const kActiveTabAlpha = 1.0f;
static CGFloat const kNonActiveTabAlpha = 0.5f;

@implementation UCTabView

+ (instancetype)tabViewWithIconName:(NSString *)iconName title:(NSString *)title tabIndex:(NSUInteger)index {
    UCTabView *tab = [[[NSBundle mainBundle] loadNibNamed:@"UCTabView" owner:nil options:nil] firstObject];
    tab.iconImageView.image = [UIImage imageNamed:iconName];
    tab->_tabIndex = index;
    if (title) {
        tab.titleLabel.text = title;
    } else {
        tab.titleLabel.hidden = YES;
    }
    return tab;
}

- (void)setActiveTab:(BOOL)activeTab {
    if (activeTab) {
        self.alpha = kActiveTabAlpha;
    } else {
        self.alpha = kNonActiveTabAlpha;
    }
    _activeTab = activeTab;
}

- (IBAction)selectTabAction:(UITapGestureRecognizer *)sender {
    [self.delegate tabSelected:self];
}

@end
