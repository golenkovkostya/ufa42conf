//
//  UCMainTabBarViewController.m
//  ufa42conf
//
//  Created by Konstantin Golenkov on 28.10.14.
//  Copyright (c) 2014 Frumatic. All rights reserved.
//

#import "UCMainTabBarViewController.h"
#import "UCTabView.h"
#import "UCNewsViewController.h"

@interface UCMainTabBarViewController () <UCTabViewProtocol> {
    NSArray *children;
    UCTabView *selectedTab;
}

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *tabCollection;

@end

@implementation UCMainTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *tabs = [NSArray arrayWithObjects:
                     [UCTabView tabViewWithIconName:@"tab-one" title:nil tabIndex:0],
                     [UCTabView tabViewWithIconName:@"tab-two" title:@"24" tabIndex:1],
                     [UCTabView tabViewWithIconName:@"tab-three" title:nil tabIndex:2],
                     [UCTabView tabViewWithIconName:@"tab-four" title:nil tabIndex:3],
                     nil];
    UCNewsViewController * vc = [UCNewsViewController newsViewController];
    
    [self.tabCollection enumerateObjectsUsingBlock:^(UIView *tabContainer, NSUInteger idx, BOOL *stop) {
        UCTabView *tab = tabs[idx];
        tab.delegate = self;
        tab.frame = tabContainer.bounds;
        [tabContainer addSubview:tab];
    }];
    
    [self tabSelected:tabs.firstObject];
}

- (void)tabSelected:(UCTabView *)tab {
    selectedTab.activeTab = NO;
    selectedTab = tab;
    selectedTab.activeTab = YES;
    
    //show child view controller
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
